CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module optimizes the sql queries for retrieving the configuration before generating the cache_config

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/optimize_config_storage

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/optimize_config_storage


REQUIREMENTS
------------

 * No specific dependancies are required


INSTALLATION
------------

 * Use `composer require drupal/optimize_config_storage` to install.


CONFIGURATION
-------------

No configuration available


MAINTAINERS
-----------

Current maintainers:
 * sofiene chaari (sofiene.chaari) - https://www.drupal.org/u/sofienechaari
